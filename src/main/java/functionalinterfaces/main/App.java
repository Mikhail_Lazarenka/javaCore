package functionalinterfaces.main;

import functionalinterfaces.interfaces.SumAgePersons;
import functionalinterfaces.person.Person;
import functionalinterfaces.person.SimplePerson;

import java.util.*;
import java.util.List;
import java.util.function.*;

public class App {
    public static void main(String[] args) {

        // 1.Create several instances of Runnable interface with different implementation using lambda expressions.
        // Use them with threads.
        System.out.println("starting task 1 \n");
        Runnable  runnable1= ()-> { System.out.println("runnable1"); };
        Runnable  runnable2=()-> {System.out.println("runnable2"); };

        Thread thread1= new Thread(runnable1);
        Thread thread2= new Thread(runnable2);

        thread1.start();
        thread2.start();

        // 2. Create class Person...
        System.out.println("starting task 2\n");
        Person person1 = new Person("Mike", 21);
        Person person2 = new Person("Anna", 30);
        Person person3 = new Person("Egor", 21);
        Person person4 = new Person("Denis", 17);

        List<Person> persons= new ArrayList<>();
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        persons.add(person4);

        Comparator<Person> ageComparator= (p1, p2) -> p1.getAge() - p2.getAge();
        persons.sort(ageComparator);
        System.out.println("After age sort\n"+persons);

        Comparator<Person> nameComparator=(p1, p2) -> p1.getName().compareToIgnoreCase(p2.getName());

        persons.sort(nameComparator);
        System.out.println("After name sort"+persons);

        //print only the name of the person
        persons.forEach((p)-> System.out.println(p.getAge()));
        //printing the whole info about person using method reference
        persons.forEach(System.out::println);

        // 3. Implement 7-9 different interfaces from java.util.function package using lambda expressions
        System.out.println("starting task 3");

        Consumer<Person> personConsumer=(person -> System.out.println(person.getName()));
        System.out.println("Using Consumer");
        personConsumer.accept(person1);


        BiConsumer<Person,Person> personPersonBiConsumer = (p1, p2 )-> System.out.println(p1.getName()+" and "+p2.getName());
        System.out.println("Using BiConsumer");
        personPersonBiConsumer.accept(person4,person2);

        Predicate<Person> isAdultPredicate = p -> p.getAge()>=18;


        System.out.println("Using Predicate \n Is adult? -"+isAdultPredicate.test(person1));

        BiPredicate<Person,Person> isPeersBiPredicate=(p1, p2) -> p1.getAge()==p2.getAge();

        System.out.println("Using BiPredicate\nIs peers? - "+isPeersBiPredicate.test(person1,person3));

        Function<Person,String> functionToString = (Person::toString);
        System.out.println("Using Functional\n");
        System.out.println(functionToString.apply(person4));

        BiFunction<Person,Person, List<Person>> listBiFunction = (p1,p2)-> {
            ArrayList<Person> list = new ArrayList<>();
            list.add(p1);
            list.add(p2);
            return list;
        };
        System.out.println("Using BiFunctional1\n"+listBiFunction.apply(person1, person4).toString());

        BiFunction<String,Integer,Person> stringIntegerPersonBiFunction =  Person::new;
        Person anna = stringIntegerPersonBiFunction.apply("Anna", 20);
        System.out.println("Using BiFunctional2\n"+anna);

        BinaryOperator<Person> integerBinaryOperator= (p1,p2)-> new Person(p1.getName()+p2.getName(),p1.getAge()+p2.getAge());
        System.out.println("Using BinaryOperator\n");
        System.out.println(integerBinaryOperator.apply(person1,person4).toString());

        //4 create your own functional interface
        System.out.println("Starting task 4\n");
        SumAgePersons sumAgePersons1=(p1,p2)-> p1.getAge()+p2.getAge();
        sumAgePersons1.sumAges(person3,person2);

        SumAgePersons sumAgePersons2= new SumAgePersons() {
            @Override
            public int sumAges(Person person1, Person person2) {
                return person1.getAge()+person2.getAge();
            }
        };

        sumAgePersons2.sumAges(person1,person4);


        ////////////////////////////////////////////////////////////////////

        int age=21;
        //effectively final
        //      age=3;
        persons.removeIf((u)->u.getAge()==age);
        persons.forEach((u)-> System.out.println(u.getAge()));

        persons.sort(Comparator.comparing(Person::getAge));



        //Using constructor reference
        Function<String,SimplePerson> stringSimplePersonFunction = SimplePerson::new;
        SimplePerson mark = stringSimplePersonFunction.apply("Mark");
        System.out.println(mark);

        List<String> personsNames= Arrays.asList("Mike","Egor","Sasha","Mike","Max","Pasha","Mike","Mike","Mike");
        List<SimplePerson> simplePeople = map(personsNames, SimplePerson::new);
        simplePeople.forEach(System.out::println);



        //Streams
        System.out.println("Streams");
        simplePeople.stream()
                .filter(p->p.getName().equalsIgnoreCase("Mike"))
                .forEach(System.out::println);


    }
    private static <S,T> List<T> map(List<S> nameList, Function<S, T>   function){
        List<T> simplePeople= new ArrayList<>(nameList.size());
        nameList.forEach(name->simplePeople.add(function.apply(name)));
        return simplePeople;

    }





}







