package functionalinterfaces.person;

public class SimplePerson {
    private String name;

    public SimplePerson(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SimplePerson{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
