package functionalinterfaces.interfaces;

import functionalinterfaces.person.Person;

@FunctionalInterface
public interface SumAgePersons {
    int sumAges(Person person1, Person person2);
}
