package classloading.entity;

public class Cat extends Animal{
    public void voice() {
        System.out.println("Cat voice");

    }

    public void play() {
        System.out.println("Cat is playing");
    }
}
