package classloading.entity;

public class Dog extends Animal {
    public void voice() {
        System.out.println("Dog voice");
    }

    public void play() {
        System.out.println("Dog is playing ");
    }
}
