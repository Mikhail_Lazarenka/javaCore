package classloading.entity;

public abstract class Animal {
    public abstract void voice();
    public abstract void play();
}
