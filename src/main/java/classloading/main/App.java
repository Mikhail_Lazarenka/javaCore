package classloading.main;

import classloading.classloader.AnimalClassLoader;
import classloading.entity.Animal;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException,
            NoSuchMethodException, SecurityException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException {

        AnimalClassLoader loader = new AnimalClassLoader();
        ArrayList<Animal> animals=  loader.loadClassesIntoList("com.epam.entity.Cat","com.epam.entity.Dog");
        for (Animal animal: animals){
            animal.play();
            animal.voice();
        }
    }
}
