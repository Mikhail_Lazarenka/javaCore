package classloading.classloader;

import classloading.entity.Animal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class AnimalClassLoader extends ClassLoader {

    @Override
    public Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] bt = loadClassData(name);
        return defineClass(name, bt, 0, bt.length);
    }

    private byte[] loadClassData(String className) {
        InputStream is = getClass().getClassLoader().getResourceAsStream(className.replace(".", "/")+".class");
        ByteArrayOutputStream byteSt = new ByteArrayOutputStream();
        int len =0;
        try {
            while((len=is.read())!=-1){
                byteSt.write(len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteSt.toByteArray();
    }

    public ArrayList<Animal> loadClassesIntoList(String pathCat, String pathDog) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class<?> cat = findClass(pathCat);
        Class<?> dog =  findClass(pathDog);
        Animal catObj = (Animal) cat.newInstance();
        Animal dogObj = (Animal) dog.newInstance();

        ArrayList<Animal> animals= new ArrayList<Animal>();
        animals.add(catObj);
        animals.add(dogObj);

        return animals;
    }
}
