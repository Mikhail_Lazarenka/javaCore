package threads.atomic;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * решение проблемы race condition путем атомиков
 *
 */
public class AtomicProblem {
    private static AtomicInteger counter= new AtomicInteger(0);
    private static int nextInt(){
        return counter.incrementAndGet();
    }

    public static void main(String[] args) throws InterruptedException {
        List<Thread> threads = new ArrayList<>();

        Runnable runnable = () -> {
            for (int j = 0; j < 1000; j++) {
                nextInt();
            }
        };
        for (int i=0; i<10; i++){
            Thread thread = new Thread(runnable);
            thread.start();
            threads.add(thread);
        }
        for (Thread thread :
             threads) {
            thread.join();
        }
        System.out.println(counter);
    }
}
