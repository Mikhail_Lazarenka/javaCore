package threads.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class App {
    private static Random random = new Random();
    public static void main(String[] args) {

        List<Integer> integers= new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            integers.add(random.nextInt());
        }

        // simple using streams вывести сумму всех нечетных значений
        //  https://habr.com/company/luxoft/blog/270383/
        Integer simpleSum=integers.stream()
                .filter(i-> i%2!=0)
                .reduce((i1,i2)->i1+i2)
                .orElse(0);


        Stream.of("a1","a2","a3").findFirst().isPresent();


        int[] ints = {1, 2, 3};
        int sum = Arrays.stream(ints).map((i) -> i + 1).sum();
        System.out.println(sum);
        Stream.of("d2", "a2", "b1", "b3", "c")
                .filter(s -> {
                    System.out.println("filter: " + s);
                    return true;
                });

        // последовательность важна

        Stream.of("d2", "a2", "b1", "b3", "c")
                .map(s -> {
                    System.out.println("map: " + s);
                    return s.toUpperCase();
                })
                .filter(s -> {
                    System.out.println("filter: " + s);
                    return s.startsWith("A");
                })
                .forEach(s -> System.out.println("forEach: " + s));

// map:     d2
// filter:  D2
// map:     a2
// filter:  A2
// forEach: A2
// map:     b1
// filter:  B1
// map:     b3
// filter:  B3
// map:     c
// filter:  C
    }
}
