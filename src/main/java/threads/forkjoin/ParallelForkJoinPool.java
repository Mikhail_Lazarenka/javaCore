package threads.forkjoin;

import threads.executorservice.Commons;

import java.util.concurrent.ForkJoinPool;

public class ParallelForkJoinPool {
    public static void main(String[] args) {
        int[] ar = Commons.prepareArray();
        ForkJoinPool forkJoinPool = new ForkJoinPool();

        long startTime=System.currentTimeMillis();
        Double sum = forkJoinPool.invoke(new RecursiveCalc(ar, 0, ar.length));
        long endTime=System.currentTimeMillis();

        long allTime=endTime-startTime;
        System.out.println("sum "+sum);
        System.out.println(allTime+"ms");
    }


}
