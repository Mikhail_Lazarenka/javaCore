package threads.forkjoin;

import threads.executorservice.Commons;

import java.util.concurrent.RecursiveTask;

public class RecursiveCalc extends RecursiveTask<Double> {
    private int[] array;
    private int start;
    private int end;
    private static final int THRESHOLD=100000;

    public RecursiveCalc(int[] array, int start, int end) {
        this.array = array;
        this.start = start;
        this.end = end;
    }


    @Override
    protected Double compute() {
        if(end-start<=THRESHOLD){
            return Commons.calculate(array,start,end);
        }else {
            int mid= (end-start)/2;
            RecursiveCalc left= new RecursiveCalc(array, start, mid);
            RecursiveCalc right = new RecursiveCalc(array, mid, array.length);
            invokeAll(left,right);
            return left.join()+right.join();
        }

    }
}
