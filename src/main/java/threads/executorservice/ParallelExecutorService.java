package threads.executorservice;

import java.util.concurrent.*;

public class ParallelExecutorService {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        int[] array = Commons.prepareArray();
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        long startTime=System.currentTimeMillis();
        Future<Double> future1 = executorService.submit(new PracticalCalc(array, 0, args.length / 2));
        Future<Double> future2 = executorService.submit(new PracticalCalc(array, array.length / 2, array.length));
        long endTime=System.currentTimeMillis();

        double sum=future2.get()+future1.get();
        System.out.println(sum);
        System.out.println(endTime-startTime+"ms");


    }

    public static class PracticalCalc implements Callable<Double>{

        private final int[] array;
        private final int statr;
        private final int end;

        public PracticalCalc(int[] array, int statr, int end) {
            this.array = array;
            this.statr = statr;
            this.end = end;
        }

        @Override
        public Double call() throws Exception {
            return Commons.calculate(array,statr,end);
        }
    }
}
