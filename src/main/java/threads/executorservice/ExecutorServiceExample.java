package threads.executorservice;

import java.util.concurrent.*;

public class ExecutorServiceExample {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Worker worker1= new Worker("Mike");
        Worker worker2= new Worker("Sasha");

        ExecutorService executorService= Executors.newFixedThreadPool(2);
        Future<String> submit1 = executorService.submit(worker1);
        Future<String> submit2 = executorService.submit(worker2);

        System.out.println("resoult from Mike: "+submit1.get());
        System.out.println("resoult from Sasha: "+submit2.get());




        try {
            executorService.shutdown();
        }finally {
            if (!executorService.isShutdown()){
                executorService.shutdownNow();
            }

        }
    }


    public static class Worker implements Callable<String>{
        private final String name;

        public Worker(String name) {
            this.name = name;
        }

        @Override
        public String call() throws Exception {

            long sleepTime= (long) (Math.random()*10000L);
            System.out.println(name + " started going to sleep for"+ sleepTime);
            Thread.sleep(sleepTime);
            System.out.println(name+" finished sleep");
            return name;
        }
    }
}
