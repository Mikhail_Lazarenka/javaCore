package threads.executorservice;

public class Sequential {
    public static void main(String[] args) {
        int[] array = Commons.prepareArray();

        long startTime=System.currentTimeMillis();
        double sum=Commons.calculate(array);
        long endTime=System.currentTimeMillis();

        long allTime=endTime-startTime;
        System.out.println("sum "+sum);
        System.out.println(allTime+"ms");
    }
}
