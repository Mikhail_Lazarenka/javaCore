package threads.stateobject;

import java.util.concurrent.atomic.AtomicInteger;

public class InterferenceExample {
    private static final int HandlerMillion =100_000_000;
    private AtomicInteger counter = new AtomicInteger();

    public boolean stop(){
        return counter.incrementAndGet()> HandlerMillion;
    }


    public   void example() throws InterruptedException {
        InterferenceThread myThread1=new InterferenceThread(this);
        InterferenceThread myThread2=new InterferenceThread(this);

        myThread1.start();
        myThread2.start();
        myThread1.join();
        System.out.println(HandlerMillion);
        System.out.println(InterferenceThread.getI());
    }

}
