package threads.stateobject;

/**
 * пример с потоками которые имею общий ресурс (статик переменную)
 * решение: нудно чтобы у каждого потока был общий лок объек
 */
public class App {
    public static void main(String[] args) throws InterruptedException {
        InterferenceExample interferenceExample= new InterferenceExample();
        interferenceExample.example();
    }
}
