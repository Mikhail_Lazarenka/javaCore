package threads.stateobject;

public class InterferenceThread extends Thread{
    private final InterferenceExample checker;
//    public static  volatile int  i=0; не работает
    public static   int  i=0;
    public static Object lock= new Object();

    public InterferenceThread(InterferenceExample checker) {
        this.checker = checker;
    }

    public static int getI() {
        return i;
    }

    public static void setI(int i) {
        InterferenceThread.i = i;
    }

    @Override
    public void run() {
        while (!checker.stop()){
            increment();
        }

    }

//    private synchronized int increment() { так тоже не работает потомучно мы лок забираем у себя а переменная у нас общая
    // первое решение это сделать статический метд! но лучше сделать статический лок объек!
    private   void increment() {
//        synchronized (lock){
        synchronized (checker){

             i++;
        }
    }
}
