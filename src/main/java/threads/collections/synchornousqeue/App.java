package threads.collections.synchornousqeue;

import java.util.concurrent.SynchronousQueue;

public class App {
    public static void main(String[] args) throws InterruptedException {
        SynchronousQueue<Integer> synchronousQueue= new SynchronousQueue<>();

        new Thread(()-> {
            try {
                Integer integer = synchronousQueue.take();
                System.out.println(integer);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        synchronousQueue.put(1);
    }
}
