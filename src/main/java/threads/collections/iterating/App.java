package threads.collections.iterating;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CopyOnWriteArrayList;

// some info about iterators
//https://www.tutorialspoint.com/java/java_using_iterator.htm
public class App {
    public static void main(String[] args) {
        final CopyOnWriteArrayList<Integer> integers= new CopyOnWriteArrayList() ;
        integers.add(1);
        integers.add(5);
        integers.add(7);
        integers.add(2);
        integers.add(9);

        new Thread(()->integers.add(3)).start();

        ListIterator<Integer> iterator = integers.listIterator();
        while (iterator.hasNext()){
            Integer next = iterator.next();
//            if (next==7)  iterator.remove();// если мы используем конкаррент коллекции то мы не може удалять через итеретор
            System.out.println(next);
        }
        System.out.println(integers);
        // нельзя дважды вызывать итератор

        System.out.println("***********");
        ListIterator<Integer> integerListIterator = integers.listIterator();
        while (integerListIterator.hasNext()){
            Integer next = integerListIterator.next();
            System.out.println(next);
        }

        System.out.println("----------");
        while (integerListIterator.hasNext()){
            Integer next = integerListIterator.next();
            System.out.println(next);
        }

        // итератор можно повторно использовать если идти в обратную сторону!
        System.out.println("***********prev");
        while (integerListIterator.hasPrevious()){
            Integer next = integerListIterator.previous();
            System.out.println(next);
        }



    }
}
