package threads.waitnotify;

public class MyThread extends Thread {
    private static volatile  int currentId=1;
    private Object lock;
    int id;

    public MyThread(Object lock, int id) {
        this.lock = lock;
        this.id = id;
    }

    @Override
    public void run() {
        synchronized (lock){
            while (id>currentId){
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
                System.out.println(id);
                currentId++;
                lock.notifyAll();

        }


    }
}
