package threads.waitnotify;

/**
 * пример того как реальзовать последоавтельный доступ к критической секции
 * через wait and notify
 */
public class App {
    public static void main(String[] args) {

        Object lock= new Object();
        for (int i = 0; i < 10; i++) {
            MyThread thread = new MyThread(lock,i);
            thread.start();

        }
    }
}
