package threads.reentantlock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account {
    private final Lock lock= new ReentrantLock();
    private final Condition balanceIncreased= lock.newCondition();

    private long balance;

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public Account(long balance) {
        this.balance = balance;
    }


    public void deposit(long amount){
        lock.lock();
        try {

            balance+=amount;
            balanceIncreased.signalAll();
        }finally {
            lock.unlock();
        }
    }

    public void waitAndWithdraw (long amount) throws InterruptedException {

        lock.lock();
        try {

            while (balance< amount){
                balanceIncreased.await();
            }
            balance-=amount;
        }finally {
            lock.unlock();

        }
    }
}
