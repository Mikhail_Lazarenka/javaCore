package threads.reentantlock;

/**
 * есть аккаунт у которое есть два метода которые засинхронизированны локам
 * к эитм методам обращаются потоки: одни пытается пополнить баланс доа 60 млин руб по 1рублю
 * а другой пытается получить 50
 */
public class LockDemo {
    public static void main(String[] args) throws InterruptedException {
        Account acount = new Account(0);
        new DepositThread(acount).start();
        acount.waitAndWithdraw(50_000_000);
        System.out.println("waitandwithdraw finished, end balance is = "+acount.getBalance());

    }
    private static class DepositThread extends Thread{
        private final Account account;


        private DepositThread(Account account) {
            this.account = account;
        }

        @Override
        public void run() {
            for (int i = 0; i < 60_000_000; i++) {
                account.deposit(1);
            }
        }
    }
}
