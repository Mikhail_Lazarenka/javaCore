package threads.semaphore;

import java.util.concurrent.Semaphore;

/**
 * пример простого семафора
 */
public class SemaphoreExampleThread extends Thread {
    private Semaphore semaphore;

    public SemaphoreExampleThread(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        while (true){
            try {
                semaphore.acquire();
                System.out.println(Thread.currentThread().getName()+" is acquired semaphore");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                semaphore.release();
            }
        }
    }

    public static void main(String[] args) {

        Semaphore  semaphore = new Semaphore(2, true);
        for (int i = 0; i < 10; i++) {
            SemaphoreExampleThread semaphoreExampleThread= new SemaphoreExampleThread(semaphore);
            semaphoreExampleThread.start();
        }


    }
}
