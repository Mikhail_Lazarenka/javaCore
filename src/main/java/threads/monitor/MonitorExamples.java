package threads.monitor;


/**
 * выюросит illefalmoitorexception
 * так как синхронайсед можно вызывать без этих методов а их самиъ нельзя
 * потомучно в эти метды вызывают нативные методы которые проверяю
 */
public class MonitorExamples {
    public static void main(String[] args) throws InterruptedException {
//1
        //        new  Object().notifyAll();
//        new  Object().wait();

        new MonitorExamples().f();

    // объект может войты в нескольнко мониторов блокировак
        Object o1= new Object();
        Object o2= new Object();
        synchronized (o1){
            synchronized (o2){
                o1.wait();
            }
        }

    }

    private synchronized void f() {
        this.notify();
    }
    public static synchronized void staticmethod(){

        Class aClass=MonitorExamples.class;
        aClass.notify();
    }
}
