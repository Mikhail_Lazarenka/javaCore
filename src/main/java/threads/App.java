package threads;

/**
 * проверяю может ли поток сам себя простановть
 */
public class App {
    public static void main(String[] args) {
        Runnable runnable = ()->{
            Thread thread = Thread.currentThread();
            String name = thread.getName();
            System.out.println("Thread "+name+" is in runnable\n");
            //self interruption
            thread.interrupt();
            try {
//                if(thread.isInterrupted()){
//                    System.out.println(name+" is interrupted " + Thread.interrupted()+"\n");
//                }
                Thread.sleep(3000);
//                Thread.interrupted();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        };

        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread1.setName("A");
        thread2.setName("B");

        thread1.start();
        thread1.interrupt();
        thread2.start();
    }

}
