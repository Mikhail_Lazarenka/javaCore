package threads.synch;

/**
 * если у нас есть два метода которые имеют синхронайз то потоки не смогут одновременно пользоваться
 * этими разными методами так как лок является сам объект
 */
public class App {
    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        MyThread myThread1 = new MyThread(myClass);
        MyThread myThread2 = new MyThread(myClass);

        myThread1.setName("A");
        myThread2.setName("B");

         myThread1.start();
         myThread2.start();



    }
}
