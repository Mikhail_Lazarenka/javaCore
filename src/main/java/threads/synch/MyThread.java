package threads.synch;

public class MyThread extends Thread {

    private MyClass myClass;

    public MyThread(MyClass myClass) {
        this.myClass = myClass;
    }

    @Override
    public void run() {
        try {
            myClass.synchronizedMethod();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        myClass.simpleMethod();
    }
}
