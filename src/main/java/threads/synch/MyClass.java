package threads.synch;

public class MyClass {
    public int i;

    public synchronized void synchronizedMethod() throws InterruptedException {

        String nameThread = Thread.currentThread().getName();
        System.out.println(nameThread+" is in the synchronized method");
        Thread.sleep(3000);
        i+=1;
    }

    public void simpleMethod(){
        String nameThread = Thread.currentThread().getName();
        System.out.println(nameThread+" is in the simple method");
        i-=1;
    }
}
