package threads.deadlock;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account {
    private AtomicInteger failCounter = new AtomicInteger();
    private Lock  lock = new ReentrantLock();
    private int balance;

    public Account(Lock lock, int balance) {
        this.lock = lock;
        this.balance = balance;
    }


    public AtomicInteger getFailCounter() {
        return failCounter;
    }

    public void setFailCounter(AtomicInteger failCounter) {
        this.failCounter = failCounter;
    }

    public Lock getLock() {
        return lock;
    }

    public void setLock(Lock lock) {
        this.lock = lock;
    }

    public Account(int balance) {
        this.balance = balance;
    }

    public void deposit(int amount){

        String threadName = Thread.currentThread().getName();
        System.out.println(threadName+" is making deposit");
        balance+=amount;
    }

    public void withdraw(int amount){
        String threadName = Thread.currentThread().getName();
        System.out.println(threadName+" is making withdraw");
        balance-=amount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "balance=" + balance +
                '}';
    }

    public void incFailTransferCount(){
        failCounter.incrementAndGet();
    }
}
