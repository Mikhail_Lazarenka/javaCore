package threads.deadlock;

import java.util.concurrent.TimeUnit;

/**
 * пример дедлока
 * решение
 * 1) иерархия ресурсов
 * 2) доп монитор
 * 3) use Lock (reentrant)
 */
public class App {
    public static void main(String[] args) throws InterruptedException {
        Account a1= new Account(1000);
        Account a2= new Account(2000);

        Thread thread = new Thread(() -> {
            try {
                transfer(a1, a2, 300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();

        transfer(a2,a1,300);


        thread.join();
        System.out.println(a1);
        System.out.println(a2);

        System.out.println(a1.getFailCounter());
        System.out.println(a2.getFailCounter());


    }

    public static void transfer(Account a1, Account a2, int amount) throws InterruptedException {
            if(a1.getLock().tryLock(1, TimeUnit.SECONDS)){
                try{
                    if(a2.getLock().tryLock(1, TimeUnit.SECONDS)){
                        try {

                            a1.withdraw(amount);
                            Thread.sleep(10);
                            a2.deposit(amount);

                        }finally {
                            a2.getLock().unlock();
                        }
                    }else {
                        a1.incFailTransferCount();
                        a2.incFailTransferCount();
                    }

                }finally {
                    a1.getLock().unlock();
                }

            }else {
                a1.incFailTransferCount();
                a2.incFailTransferCount();
            }


    }
}
