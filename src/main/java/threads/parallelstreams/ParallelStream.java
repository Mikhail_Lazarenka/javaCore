package threads.parallelstreams;

import threads.executorservice.Commons;

import java.util.Arrays;

public class ParallelStream {
    public static void main(String[] args) {
        int[] array = Commons.prepareArray();
        long startTime=System.currentTimeMillis();
        double sum=Arrays.stream(array)
                .parallel()
                .mapToDouble(Math::sin)
                .sum();
        long endTime=System.currentTimeMillis();

        long allTime=endTime-startTime;
        System.out.println("sum "+sum);
        System.out.println(allTime+"ms");
    }
}
